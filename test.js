

//ファイルのメインソースコード

var tilename = new Array("一萬", "二萬", "三萬", "四萬", "五萬","六萬","七萬","八萬","九萬",
		"一筒", "二筒", "三筒", "四筒", "五筒","六筒","七筒","八筒","九筒",
		"一索", "二索", "三索", "四索", "五索","六索","七索","八索","九索",
		"東", "南", "西", "北", "白", "發", "中");
var tileIndx = new Array();
var tileIndx_cnt = 0;
for(tileIndx_cnt = 0; tileIndx_cnt < tilename.length; tileIndx_cnt ++){
	tileIndx[tilename[tileIndx_cnt]] = tileIndx_cnt;
}

var hand = new Array( 14 ); //手牌を意味する配列変数
var handmax = 14;
var hand_selflg = -1;		//選択された牌を示す変数

//*****手牌のリセット****
function resetHand(){
	for( var i = 0; i < 14; i++ ) {
		hand[i] = 34;
	}
}

//*****ドラリセット*****//
var dragons = new Array(16);
function resetDragons(){
	for( var i = 0; i < dragons.length; i++ ) {
		dragons[i] = 34;
	}
}

//*****副露リセット******//
var furos = new Array(handmax - 1);
function resetFuros(){
	for( var i = 0; i < furos.length; i++ ) {
		furos[i] = 0;
	}
}

//*****理牌*****//
function sortHands(){
	var temp = hand[hand.length - 1];
	hand[hand.length - 1] = tilemax;
	hand.sort(
		function(a,b){
			if( a < b ) return -1;
	        if( a > b ) return 1;
		}
	);
	hand[hand.length - 1] = temp;
}

//*****山牌から手牌候補を選ぶ*****//
function select_hands(){
	if( click[1] === 1 ) {
		if( hand_selflg === -1 ) {
			hand_selflg = get_HandSelnum();
			return;
		}
	} else if( click[1] === -1 ) {
		var canvas = document.getElementById('calc');
		//特に手牌部分にマウスカーソルがあるとき
		var handstart_x = 0;
		var handstart_y = canvas.height - tiley;
		if( hand_selflg >= 0 && mousepos[0] >= handstart_x && mousepos[0] < handstart_x + tilex * handmax &&
				mousepos[1] >= handstart_y && mousepos[1] < handstart_y + tiley ) {
			hand[ Math.floor( ( mousepos[0] - handstart_x ) / tilex ) ] = hand_selflg;
		} else if( hand_selflg >= 0 && mousepos[0] >= canvas.width - tilex * 4 && mousepos[0] < canvas.width
				&& mousepos[1] >= 0 && mousepos[1] < 4 * tiley ) {
			dragons[Math.floor( ( mousepos[0] - canvas.width + tilex * 4 ) / tilex ) + 4 * Math.floor( mousepos[1] / tiley ) ] = hand_selflg;
		}
		hand_selflg = -1;
		return;
	}
}

//*****副露のセット*****//
var prevstate;
function setFuro(){
	if( hand_selflg !== -1 ) return;
	var canvas = document.getElementById('calc');
	if( prevstate === -1 && click[1] === 1 ) {
		if( mousepos[0] < 0 || mousepos[0] >= tilex * furos.length ||
				mousepos[1] < canvas.height - tiley - furoy || mousepos[1] >= canvas.height - tiley )
			{ return; }
		furos[ Math.floor( mousepos[0]  / tilex ) ] ++;
		if( furos[ Math.floor( mousepos[0]  / tilex )] > 7 ) furos[ Math.floor( mousepos[0]  / tilex )] = 0;
	}
	prevstate = click[1]
}

//*****マウスのグローバル座標を取得する*****//
/*	@param
 *
 */
var mousepos = new Array(2);
document.onmousemove = function ( e ) {
	var x = document.body.scrollLeft || document.documentElement.scrollLeft;
	var y = document.body.scrollTop  || document.documentElement.scrollTop;
	if( !e ) e = window.event;
	x += e.clientX;
	y += e.clientY;
	mousepos[0] = x;
	mousepos[1] = y;
}

//******マウスがcanvasないにあるかどうか判定する******//
function isInCanvas(){
	var canvas = document.getElementById('calc');
	if( mousepos[0] < 0 || mousepos[0] >= canvas.width || mousepos[1] < 0 || mousepos[1] >= canvas.height )
		return false;
	return true;
}

var click = new Array(4);
click[0] = 0;
click[1] = 0;
click[2] = 0;
click[3] = 0;

function getMouseState() {
	var canvas = document.getElementById('calc');
	if(!canvas) return;
	canvas.onmousedown = function( e ) {
		if(!e) e = window.event;
		if( click[e.which] === -1 ) {
			click[e.which] = 1;
		} else if( click[e.which] === 1 ) {
			click[e.which] = 2;
		}
	};
	canvas.onmouseup = function( e ) {
		if(!e) e = window.event;
		click[e.which] = -1;
	};

}

//牌山から牌を選択する
function get_HandSelnum(){
	var x, y;
	x = mousepos[0];
	y = mousepos[1];
	if( x < 0 || x >= tilex * 10 || y < 0 || y >= tiley * 4 )
		return -1;
	var ret = Math.floor( x / tilex ) +  Math.floor( y / tiley ) * 9 ;
	if( ret >= tilemax ) return -1;

	return ret;
}

/*	■■■■■■■■■■■■■■■■■■■■■
 *	■■■■■■描画関係関数ここから■■■■■
 *	■■■■■■■■■■■■■■■■■■■■■
 */

//********画像読み込み*********
var tileimg = new Image();
var furoimg = new Image();
var tilex = 32;
var tiley = 45;
var furox = 32;
var furoy = 19;
var tilemax = 34;
var handmax = 14;
function load(){
	var canvas = document.getElementById('calc');
	if( canvas.getContext ) {
		//コンテキストの取得
		var ctx = canvas.getContext('2d');
		//高さと幅の初期化
		tileimg.height = 288;
		tileimg.width = 180;
		tileimg.src = "tiles.png";
		furoimg.height = 38;
		furoimg.width = 160;
		furoimg.src = "furo.png"
	}
	resetHand();
	resetDragons();
	resetFuros();
	setYaku();
}

/*
 * //牌画像の描画
 * @var
 * gh:牌のＩＤ
 * x:描画先のx座標
 * y:描画先のy座標
*/
function drawTiles( gh, x, y ) {
	var canvas = document.getElementById('calc');
	if( !canvas || !canvas.getContext ) { return; }
	var ctx = canvas.getContext('2d');
	var posx = Math.floor( gh % 9 );
	var posy = Math.floor( gh / 9 );
	ctx.drawImage( tileimg,  posx * tilex , posy * tiley, tilex, tiley, x, y, tilex, tiley );
}

/*
 * 副露部分の描画
 */
function drawFuro( gh, x, y ) {
	var canvas = document.getElementById('calc');
	if( !canvas || !canvas.getContext ) { return; }
	var ctx = canvas.getContext('2d');
	var posx = Math.floor( gh % 5 );
	var posy = Math.floor( gh / 5 );
	ctx.drawImage( furoimg, posx * furox, posy * furoy, furox, furoy, x, y, furox, furoy);
}

//*****ドラの描画*****
function drawDragons() {
	var canvas = document.getElementById('calc');
	var dragonsStartX = canvas.width - tilex * 4;
	for( var i = 0; i < dragons.length; i++ ) {
		var posx = Math.floor( i % 4 ) * tilex + dragonsStartX;
		var posy = Math.floor( i / 4 ) * tiley;
		drawTiles( dragons[i], posx, posy );
	}
}

//********牌山の描画*******
function drawPile(){
	var canvas = document.getElementById('calc');
	var ctx = canvas.getContext('2d');
	var i = 0;
	for( i = 0; i < tilemax; i++ ) {
		drawTiles( i, tilex * ( i % 9 ), tiley * Math.floor ( i / 9 ) );
	}
}

//*********手牌の描画*********
function drawHand(){
	var handstart_x = 0;
	var canvas = document.getElementById('calc');
	var handstart_y = canvas.height - tiley;

	var ctx = canvas.getContext('2d');
	for( var i = 0; i < handmax; i++ ) {
		drawTiles( hand[i], handstart_x + i * tilex, handstart_y );
	}

	for( var i = 0; i < furos.length; i++ ) {
		drawFuro( furos[i], i * tilex, handstart_y - furoy );
	}
}

//********選択牌の描画********
function drawSelectedTile(){
	if( hand_selflg === -1 ) return;
	if( isInCanvas() === false) return;
	var posx = mousepos[0] - tilex / 2;
	var posy = mousepos[1] - tiley / 2;
	drawTiles( hand_selflg, posx, posy );
}

//*********描画*********
function draw(){
	var canvas = document.getElementById('calc');
	if(!canvas){ return; }
	var ct = canvas.getContext('2d');
	if(!ct){ return; }
	//背景の描画
	ct.fillStyle = '#114514';
	ct.fillRect( 0, 0, canvas.width, canvas.height );
	drawPile();				//牌山の描画
	drawHand();				//手牌の描画
	drawDragons();			//ドラの描画
	drawSelectedTile();		//選択牌の描画
}

//*********再描画*********
function redraw(){
	var canvas = document.getElementById('calc');
	if(!canvas) { return; }
	var ct = canvas.getContext('2d');
	ct.clearRect(0,0,canvas.width,canvas.height);
}

/*	■■■■■■■■■■■■■■■■■■■■■
 *	■■■■■■描画関係関数ここまで■■■■■
 *	■■■■■■■■■■■■■■■■■■■■■
 */

var machiName = new Array("両面待ち", "双碰待ち", "嵌張待ち", "辺張待ち", "単騎待ち");
var machiIndx = new Array();
machiIndx["両面待ち"] = 0;
machiIndx["双碰待ち"] = 1;
machiIndx["嵌張待ち"] = 2;
machiIndx["辺張待ち"] = 3;
machiIndx["単騎待ち"] = 4;

/***********手牌オブジェクト***********/
function possibleHands( tehai, agaritile ){
	this.flg = new Array(yakucnt);			//各役が成立しているかどうか判定するフラグ
	this.pts = 0;							//点数(基礎点数)
	this.han = 0;							//翻数
	this.fu  = 0;							//符
	this.isYakuman = false;					//役満が成立しているかどうか
	this.isFuro = false;					//副露されているかどうか
	this.agari = agaritile;					//上がり牌がどの種類の牌であるか
	this.janto;								//雀頭がどの種類の牌であるか
	this.freq = new Array(tilemax);		//牌の出現頻度
	for( var i = 0; i < this.flg.length; i++ ) {
		this.flg[i] = false;
	}
	for( var i = 0; i < this.freq.length; i++ ) {
		this.freq[i] = 0;
	}
	for( var i = 0; i < tehai.length; i++ ) {
		this.freq[tehai[i]] ++;
	}
	this.hand = new Array(tehai.length);
	for( var i = 0; i < tehai.length; i++ ) {
		this.hand[i] = tehai[i];
	}
					//手牌配列
	this.furoCnt = 0;				//副露回数
	this.shuntsu = new Array();		//順子の左端の牌
	this.anshuntsu = new Array();	//暗順子
	this.minko = new Array();		//明刻
	this.anko = new Array();		//暗刻
	this.minkan = new Array();		//明槓
	this.ankan = new Array();		//暗槓
	this.kotsu = new Array();		//刻子
	this.kantsu = new Array();		//槓子
	this.machi = 0;					//待ちの種類
	this.fustr = "";				//符の内訳表現の文字列
	this.hanstr = "";				//翻の内訳表現の文字列
	this.ptsstr = "";				//点数内訳表現の文字列
	this.machiTileStr = "";			//待ち牌表現の文字列
}

/*	***********************************************
 *	******手牌から和了形を読み取る関数*************
 *	***********************************************
 */

function showHands(){
	searchHands( false );
}

var possible;
var chkBoxState = new Array(document.chkbox.elements.length);			//チェックボックスの情報を格納する配列変数
var chkBoxState_locals = new Array(document.local.elements.length);
var chkBoxState_options = new Array(document.options.elements.length);
var winds = new Array(2);					//場風・自風をあらわす配列
function searchHands( showflg ){

	sortHands();

	possible = new Array();					//手牌候補をあらわす配列
	var freqs = new Array( tilemax + 1 );	//手牌出現頻度
	//チェックボックスの情報を取得する
	for( var i = 0; i < chkBoxState.length; i++ ) {
		if( document.chkbox.elements[i].checked ) {
			chkBoxState[i] = document.chkbox.elements[i].value;
		} else {
			chkBoxState[i] = 0;
		}
	}
	//ローカル役チェックボックスの情報を取得する
	for( var i = 0; i < chkBoxState_locals.length; i++ ) {
		if( document.local.elements[i].checked ) {
			chkBoxState_locals[i] = true;
		} else {
			chkBoxState_locals[i] = false;
		}
	}
	//オプションチェックボックスの情報を取得する
	for( var i = 0; i < chkBoxState_options.length; i++) {
		if( document.options.elements[i].checked ) {
			chkBoxState_options[i] = true;
		} else {
			chkBoxState_options[i] = false;
		}
	}
	//セレクトボックスの情報を取得する
	winds[0] = document.wind.field.selectedIndex;
	winds[1] = document.wind.player.selectedIndex;

	//面子構成を読み取る
	var p_hand = new possibleHands(hand, hand[hand.length - 1] );
	//*******国士無双*****//
	var kokushi = new Array(0,8,9,17,18,26,27,28,29,30,31,32,33);	//国士無双の牌ID
	for( var i = 0; i < kokushi.length; i++ ) {
		p_hand.freq[ kokushi[ i ] ] --;
	}
	for( var i = 0; i < kokushi.length; i++ ) {
		if( p_hand.freq[ kokushi[ i ] ] < 0 || p_hand.freq[kokushi[i] > 1 ] || p_hand.isFuro === true ) {
			break;
		}
		if( i === kokushi.length - 1 ) {
			p_hand.isYakuman = true;
			if( p_hand.freq[p_hand.agari] === 1 && ( p_hand.agari % 9 === 0 || p_hand.agari % 9 === 8 || Math.floor( p_hand.agari / 9 ) === 3 ) ) {
				p_hand.flg[ yakuIndx["国士無双十三面待ち"] ] = true;
				p_hand.machi = machiIndx["単騎待ち"];
				possible.push( p_hand );
			} else if( ( p_hand.agari % 9 === 0 || p_hand.agari % 9 === 8 || Math.floor( p_hand.agari / 9 ) === 3 ) ){
				p_hand.flg[ yakuIndx["国士無双"] ] = true;
				p_hand.machi = machiIndx["単騎待ち"];
				possible.push( p_hand );
			}

		}
	}

	//******十三不塔*******//
	if( chkBoxState_locals[0] === true ) {
		//対子が存在する場合
		if( true ) {
			var p_hand = new possibleHands( hand, hand[hand.length - 1] );
			var thirteens = new Array(tilemax);
			var dh = new Array(-2, -1, 0, 1, 2);
			for( var i = 0; i < thirteens.length; i++ ) {
				thirteens[i] = 0;
			}
			p_hand.janto = -1;
			for( var i = 0; i < p_hand.freq.length; i++ ) {
				if( p_hand.freq[i] ===  2 ) {
					p_hand.freq[i] -= 1;
					p_hand.janto = i;
				}
			}

			if( p_hand.janto !== -1 ) {
				p_hand.flg[yakuIndx["十三不塔"]] = true;
				for( var i = 0; i < p_hand.freq.length; i++ ) {
					if( p_hand.freq[i] > 1 ) {
						p_hand.flg[yakuIndx["十三不塔"]] = false;
						break;
					} else if( p_hand.freq[i] === 1 ) {
						if( thirteens[i] !== 0 ) {
							p_hand.flg[yakuIndx["十三不塔"]] = false;
							break;
						}
						var color = Math.floor( i / 9 );
						for( var j = 0; j < dh.length; j++ ) {
							if( color >= 3 && dh[j] !== 0 ) { continue; }
							if( i % 9 + dh[j] < 0 ||  i % 9 + dh[j] > 8 ) { continue; }
							thirteens[ i + dh[j] ] ++;
						}
					}
				}
				if( p_hand.flg[yakuIndx["十三不塔"]] === true ) {
					p_hand.isYakuman = true;
					possible.push( p_hand );
				}
			}
		}

		//対子が存在しない場合
		if( p_hand.flg[yakuIndx["十三不塔"]] === false ) {
			var p_hand = new possibleHands( hand, hand[hand.length - 1] );
			var thirteens = new Array(tilemax);
			var dh = new Array(-2, -1, 0, 1, 2);
			for( var i = 0; i < thirteens.length; i++ ) {
				thirteens[i] = 0;
			}
			p_hand.flg[yakuIndx["十三無靠"]] = true;
			for( var i = 0; i < p_hand.hand.length; i++ ) {
				var color = Math.floor( p_hand.hand[i] / 9 );

				if( thirteens[p_hand.hand[i]] !== 0 ) {
					p_hand.flg[yakuIndx["十三無靠"]] = false;
					break;
				}

				for( var j = 0; j < dh.length; j++ ) {
					if( color >= 3 && dh[j] !== 0 ) { continue; }
					if( p_hand.hand[i] % 9 + dh[j] < 0 ) { continue; }
					if( p_hand.hand[i] % 9 + dh[j] > 8 ) { continue; }
					thirteens[ p_hand.hand[i] + dh[j] ] ++;
				}
			}
			if( p_hand.flg[yakuIndx["十三無靠"]] === true ) {
				p_hand.isYakuman = true;
				possible.push( p_hand );
			}
		}

	}



	//******七対子*******//
	var p_hand = new possibleHands( hand, hand[hand.length - 1] );
	var sevenpairs_count = 0;
	for( var i = 0; i < p_hand.freq.length ; i++ ) {
		if( p_hand.freq[i] === 2 ) {
			sevenpairs_count ++;
		}
	}
	if( sevenpairs_count === 7 ) {
		p_hand.flg[ yakuIndx["七対子"] ] = true;
		p_hand.machi = machiIndx["単騎待ち"];
		if( p_hand.isFuro === true ) p_hand.flg[ yakuIndx["七対子"] ] = false;
		possible.push(p_hand);
	}

	var p_hand = new possibleHands( hand, hand[hand.length - 1] );
	//*****流し満貫******//
	if( chkBoxState[10] === '1' ) {
		p_hand.flg[yakuIndx["流し満貫"]] = true;
		possible.push(p_hand);
	}

	//******一般形*******//

	//雀頭ループ
	for( var cnt0 = 0; cnt0 < tilemax; cnt0++ ) {
		//順子優先・刻子優先・順子１と刻子１ループ
		if( freqs[cnt0] < 2 ) { continue; }
		for( var cnt1 = 0; cnt1 < 3; cnt1 ++ ) {

			//オブジェクト生成
			var p_hand = new possibleHands( hand, hand[hand.length - 1] );

			//副露処理

			//チーを処理する
			for( var i = 0; i < furos.length; i++ ) {
				if( furos[i] === 0 || furos[i] > 4 ) { continue; }
				if( !( p_hand.freq[hand[i]] > 0 && p_hand.freq[ hand[i] +1 ] > 0 && p_hand.freq[ hand[i] + 2 ] > 0 ) )
					{continue;}
				p_hand.isFuro = true;
				p_hand.shuntsu.push( hand[i] );
				p_hand.freq[ hand[i] ] --;
				p_hand.freq[ hand[i] + 1 ] --;
				p_hand.freq[ hand[i] + 2 ] --;
				p_hand.furoCnt ++;	//副露カウンタを進める
			}
			//ポン・カンを処理する
			for( var i = 0; i < furos.length; i++ ) {
				if( furos[i] < 5 ) { continue; }
				if( p_hand.freq[hand[i]] < 3 ) { continue; }
				if( furos[i] !== 6 ) { p_hand.isFuro = true; }
				if( furos[i] === 5 ) p_hand.minko.push(hand[i]);
				if( furos[i] === 6 ) p_hand.ankan.push(hand[i]);
				if( furos[i] === 7 ) p_hand.minkan.push(hand[i]);
				p_hand.kotsu.push(hand[i]);
				p_hand.freq[hand[i]] -= 3;
				if( furos[i] !== 6 ) { p_hand.furoCnt ++; }	//副露カウンタを進める
			}

			//雀頭を取り出す
			p_hand.janto = cnt0;
			p_hand.freq[cnt0] -= 2;
			if( freqs[cnt0] < 0 ) { continue; }


			if( cnt1 === 0 ) {
				//順子を先に取り尽くす
				for( var i = 0; i < tilemax; i++ ) {
					if( i % 9 > 6 || Math.floor( i / 9 ) === 3 ) { continue; }
					while( p_hand.freq[i] > 0 && p_hand.freq[i + 1] > 0 && p_hand.freq[i+2] > 0 ) {
						p_hand.shuntsu.push(i);
						p_hand.anshuntsu.push(i);
						p_hand.freq[i] --;
						p_hand.freq[i + 1] --;
						p_hand.freq[i + 2] --;
					}
				}
				//刻子を取り出す
				for( var i = 0; i < tilemax; i++ ) {
					while( p_hand.freq[i] >= 3 ) {
						p_hand.kotsu.push(i);
						if( ( i === p_hand.agari && chkBoxState[3] === '1'  ) || i !== p_hand.agari || p_hand.freq[i] === 4 ) { p_hand.anko.push(i); }
						else { p_hand.minko.push(i); }
						p_hand.freq[i] -= 3;
					}
				}


			} else if( cnt1 === 1 ) {
				//刻子を先に取り尽くす
				for( var i = 0; i < tilemax; i++ ) {
					while( p_hand.freq[i] >= 3 ) {
						p_hand.kotsu.push(i);
						if( ( i === p_hand.agari && chkBoxState[3] === '1'  ) || i !== p_hand.agari || p_hand.freq[i] === 4 ) { p_hand.anko.push(i); }
						else { p_hand.minko.push(i); }
						p_hand.freq[i] -= 3;
					}
				}
				for( var i = 0; i < tilemax; i++ ) {
					if( i % 9 > 6 || Math.floor( i / 9 ) === 3 ) { continue; }
					while( p_hand.freq[i] > 0 && p_hand.freq[i + 1] > 0 && p_hand.freq[i+2] > 0 ) {
						p_hand.shuntsu.push(i);
						p_hand.anshuntsu.push(i);
						p_hand.freq[i] --;
						p_hand.freq[i + 1] --;
						p_hand.freq[i + 2] --;
					}
				}


			} else if( cnt1 === 2 ) {
				//順子を一つ取り出し、刻子を取り尽くす
				for( var i = 0; i < tilemax; i++ ) {
					if( i % 9 > 6 || Math.floor( i / 9 ) === 3 ) { continue; }
					if( p_hand.freq[i] > 0 && p_hand.freq[i + 1] > 0 && p_hand.freq[i+2] > 0 ) {
						p_hand.shuntsu.push(i);
						p_hand.anshuntsu.push(i);
						p_hand.freq[i] --;
						p_hand.freq[i + 1] --;
						p_hand.freq[i + 2] --;
					}
				}
				for( var i = 0; i < tilemax; i++ ) {
					while( p_hand.freq[i] >= 3 ) {
						p_hand.kotsu.push(i);
						if( ( i === p_hand.agari && chkBoxState[3] === '1'  ) || i !== p_hand.agari || p_hand.freq[i] === 4 ) { p_hand.anko.push(i); }
						else { p_hand.minko.push(i); }
						p_hand.freq[i] -= 3;
					}
				}
			}


			//和了判定
			for( var i = 0; i < p_hand.freq.length; i++ ) {
				if( p_hand.freq[i] !== 0 ) break;
				if( p_hand.freq.length - 1 === i ) {
					possible.push( p_hand );
				}
			}

		}
	}

	//手牌候補のうち、最大の点数・翻数をもつインデックスを調べる
	var maxPtsIndx = 0;
	for(var i = 0; i < possible.length; i++ ) {
		checkMachi(possible[i]);
		checkHands(possible[i]);
		checkLocalHands(possible[i]);
		checkYakuCompounded(possible[i]);
		getFu(possible[i]);
		checkYakuman(possible[i]);
		checkNagashi(possible[i]);
		getHan(possible[i]);
		getPts(possible[i]);
		resetObjFreq(possible[i]);
		if( possible[i].pts > possible[maxPtsIndx].pts || ( possible[i].pts ===  possible[maxPtsIndx].pts && possible[i].han > possible[maxPtsIndx].han ) ) {
			maxPtsIndx = i;
		}
	}

	if( showflg === true ) {
		var ret = ( possible.length > 0 );
		if( ret === true ) {
			possibleList.push( possible[maxPtsIndx]);
		}
		return( ret );
	}

	if( possible.length === 0 ) {
		document.outputbox.outbox.value = "和了形なし\n";
		return;
	}

	var outstr = possible[maxPtsIndx].ptsstr + "\n" + possible[maxPtsIndx].hanstr + "\n" +  possible[maxPtsIndx].fustr;
	document.outputbox.outbox.value = outstr;

}

/*	***********************************************
 *	******和了形から待ちを読み取る関数*************
 *	***********************************************
 */


function checkMachi( obj ) {
	//待ちを検出する

	obj.machi = -1;

	//嵌張待ち
	for( var i = 0; i < obj.anshuntsu.length; i++ ) {
		if( obj.agari === obj.anshuntsu[i] + 1 ) {
			obj.machi = machiIndx["嵌張待ち"];
		}
	}

	//辺張待ち
	for( var i = 0; i < obj.anshuntsu.length; i++ ) {
		if( ( obj.agari % 9 === 2 && obj.anshuntsu[i] % 9 === 0 ) ||
				obj.agari % 9 === 6 && obj.anshuntsu[i] % 9 === 6 ) {
			obj.machi = machiIndx["辺張待ち"];
		}
	}

	//単騎待ち
	for( var i = 0; i < obj.freq.length; i++ ) {
		obj.freq[i] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++ ) {
		obj.freq[obj.hand[i]] ++;
	}
	for( var i = 0; i < obj.shuntsu.length; i++) {
		obj.freq[obj.shuntsu[i]] --;
		obj.freq[obj.shuntsu[i] + 1] --;
		obj.freq[obj.shuntsu[i] + 2] --;
	}
	for( var i = 0; i < obj.kotsu.length; i++) {
		obj.freq[obj.kotsu[i]] -= 3;
	}
	if( obj.freq[obj.agari] === 2 && obj.agari === obj.janto ) {
		obj.machi = machiIndx["単騎待ち"];
	}

	//双碰待ち
	if( obj.agari !== obj.janto ) {
		for( var i = 0; i < obj.minko.length; i++ ) {
			if( obj.agari === obj.minko[i] ) {
				obj.machi = machiIndx["双碰待ち"];
			}
		}
	}

	//両面待ち
	if( obj.machi === -1 ) {
		for( var i = 0; i < obj.anshuntsu.length; i++ ) {
			if( obj.anshuntsu[i] === obj.agari || obj.anshuntsu[i] + 2 === obj.agari ) {
				obj.machi = machiIndx["両面待ち"];
			}
		}
	}
}

/*	***********************************************
 *	******和了形から役を読み取る関数***************
 *	***********************************************
 */

//引数には和了形候補型オブジェクトが入る
function checkHands( obj ){

	//********一翻役********//
	//立直
	if( chkBoxState[0] === '1' ) {
		obj.flg[yakuIndx["立直"]] = true;
	}
	//一発
	if( ( chkBoxState[0] === '1' || chkBoxState[1] === '1' ) && chkBoxState[2] === '1' ) {
		obj.flg[yakuIndx["一発"]] = true;
	}
	//門前清自摸和
	if( chkBoxState[3] === '1' ) {
		obj.flg[yakuIndx["門前清自摸和"]] = true;
	}
	//断么九
	if( chkBoxState[11] === '1' ) {
		yakulist[yakuIndx["断么九"]].downpts = 0;
	} else {
		yakulist[yakuIndx["断么九"]].downpts = yakulist[yakuIndx["断么九"]].points;
	}
	for( var i = 0; i < obj.hand.length; i++ ) {
		if( obj.hand[i] % 9 === 0 || obj.hand[i] % 9 === 8 || Math.floor( obj.hand[i] / 9 ) === 3 ) {
			break;
		}
		if( i === obj.hand.length - 1) {
			obj.flg[yakuIndx["断么九"]] = true;
		}
	}
	//平和
	for( var i = 0; i < obj.freq.length; i++){
		obj.freq[obj.hand[i]] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++){
		obj.freq[obj.hand[i]] ++;
	}
	obj.freq[obj.janto] -= 2;
	if( obj.kotsu.length === 0 ) {
		//手牌が全て順子
		if( obj.janto !== tileIndx["東"] + winds[0] && obj.janto !== tileIndx["東"] + winds[1] && obj.janto < tileIndx["白"]) {
			//雀頭が役牌でない
			if( true ) {
				//門前である
				var mati = false;
				if( obj.freq[ obj.agari ] > 0 ) {
					//両面待ちである
					//平和が成立するなら両面待ちとして解釈する
					//平和として解釈できない場合、可能ならば他の待ちとして解釈する
					for( var i = 0; i < obj.shuntsu.length; i++ ) {
						if( obj.shuntsu[i] === obj.agari || obj.shuntsu[i] + 2 === obj.agari ) {
							mati = true;
							//辺張を排除する
							if( ( ( obj.agari % 9 === 2 ) && ( obj.shuntsu[i] % 9 === 0 ) ) ||
									( ( obj.agari % 9 === 6 ) && ( obj.shuntsu[i] % 9 === 6 ) ) ) {
								mati = false;
							}
						}
						if( mati === true ) break;
					}
					if( mati === true ) {
						obj.machi = machiIndx["両面待ち"];
						obj.flg[yakuIndx["平和"]] = true;
					}
				}
			}
		}
	}
	//一盃口
	if( obj.shuntsu.length > 1 ) {
		for( var i = 0; i < obj.shuntsu.length; i++ ) {
			for( var j = i + 1; j < obj.shuntsu.length; j++ ) {
				if( obj.shuntsu[i] === obj.shuntsu[j] ) {
					obj.flg[yakuIndx["一盃口"]] = true;
				}
			}
		}
	}
	for( var i = 0; i < obj.freq.length; i++){
		obj.freq[obj.hand[i]] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++){
		obj.freq[obj.hand[i]] ++;
	}
	//場風
	if( obj.kotsu.length > 0 ) {
		if( obj.freq[ tileIndx["東"] + winds[0] ] >= 3 ) {
			obj.flg[yakuIndx["場風　東"] + winds[0] ] = true;
		}
	}
	//自風
	if( obj.kotsu.length > 0 ) {
		if( obj.freq[ tileIndx["東"] + winds[1] ] >= 3 ) {
			obj.flg[yakuIndx["自風　東"] + winds[1] ] = true;
		}
	}
	//三元牌
	if( obj.kotsu.length > 0 ) {
		for( var i = 0; i < 3; i++ ) {
			if( obj.freq[ tileIndx["白"] + i ] >= 3 ) {
				obj.flg[ yakuIndx["三元牌　白"] + i ] = true;
			}
		}
	}
	//嶺上開花
	if( chkBoxState[4] === '1' ) {
		obj.flg[ yakuIndx["嶺上開花"]] = true;
	}
	//槍槓
	if( chkBoxState[5] === '1' ) {
		obj.flg[ yakuIndx["槍槓"]] = true;
	}
	//海底摸月
	if( chkBoxState[6] === '1' &&  chkBoxState[3] === '1' &&  chkBoxState[4] !== '1' ) {
		obj.flg[ yakuIndx["海底摸月"]] = true;
	}
	//河底撈魚
	if( chkBoxState[7] === '1' && chkBoxState[3] !== '1' ) {
		obj.flg[ yakuIndx["河底撈魚"]] = true;
	}

	//*******二翻役*******//
	//三色同順
	if( obj.shuntsu.length === 3 ) {
		if( obj.shuntsu[0] % 9 === obj.shuntsu[1] % 9 && obj.shuntsu[0] % 9 === obj.shuntsu[2] % 9) {
			if( Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[1] / 9 )
					&& Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )
					&& Math.floor( obj.shuntsu[1] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )) {
				obj.flg[ yakuIndx["三色同順"]] = true;
			}
		}
	} else if( obj.shuntsu.length === 4 ) {
		//012
		if( obj.shuntsu[0] % 9 === obj.shuntsu[1] % 9 && obj.shuntsu[0] % 9 === obj.shuntsu[2] % 9) {
			if( Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[1] / 9 )
					&& Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )
					&& Math.floor( obj.shuntsu[1] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )) {
				obj.flg[ yakuIndx["三色同順"]] = true;
			}
		//013
		}
		if( obj.shuntsu[0] % 9 === obj.shuntsu[1] % 9 && obj.shuntsu[0] % 9 === obj.shuntsu[3] % 9) {
			if( Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[1] / 9 )
					&& Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )
					&& Math.floor( obj.shuntsu[1] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )) {
				obj.flg[ yakuIndx["三色同順"]] = true;
			}
		//023
		}
		if( obj.shuntsu[0] % 9 === obj.shuntsu[2] % 9 && obj.shuntsu[0] % 9 === obj.shuntsu[3] % 9) {
			if( Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )
					&& Math.floor( obj.shuntsu[0] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )
					&& Math.floor( obj.shuntsu[2] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )) {
				obj.flg[ yakuIndx["三色同順"]] = true;
			}
		//123
		}
		if( obj.shuntsu[1] % 9 === obj.shuntsu[2] % 9 && obj.shuntsu[1] % 9 === obj.shuntsu[3] % 9) {
			if( Math.floor( obj.shuntsu[1] / 9 ) !== Math.floor( obj.shuntsu[2] / 9 )
					&& Math.floor( obj.shuntsu[1] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )
					&& Math.floor( obj.shuntsu[2] / 9 ) !== Math.floor( obj.shuntsu[3] / 9 )) {
				obj.flg[ yakuIndx["三色同順"]] = true;
			}
		}
	}

	//一気通貫
	if( obj.shuntsu.length === 3 ) {
		//順子三組の色が同じ
		if( Math.floor( obj.shuntsu[0] / 9 ) ===  Math.floor( obj.shuntsu[1] / 9 ) && Math.floor( obj.shuntsu[0] / 9 ) === Math.floor( obj.shuntsu[2] / 9 ) ) {
			var cnt = 0;
			var cntArray = new Array(0,1,2);
			for( var i = 0; i < cntArray.length; i++ ) {
				if( obj.shuntsu[cntArray[i]] % 9 === 0 ) cnt += 1;
				if( obj.shuntsu[cntArray[i]] % 9 === 3 ) cnt += 10;
				if( obj.shuntsu[cntArray[i]] % 9 === 6 ) cnt += 100;
			}
			if( cnt === 111 ) {
				obj.flg[ yakuIndx["一気通貫"]] = true;
			}
		}
	} else if( obj.shuntsu.length === 4 ) {
		if( Math.floor( obj.shuntsu[0] / 9 ) ===  Math.floor( obj.shuntsu[1] / 9 ) && Math.floor( obj.shuntsu[0] / 9 ) === Math.floor( obj.shuntsu[2] / 9 ) ) {
			var cnt = 0;
			var cntArray = new Array(0,1,2);
			for( var i = 0; i < cntArray.length; i++ ) {
				if( obj.shuntsu[cntArray[i]] % 9 === 0 ) cnt += 1;
				if( obj.shuntsu[cntArray[i]] % 9 === 3 ) cnt += 10;
				if( obj.shuntsu[cntArray[i]] % 9 === 6 ) cnt += 100;
			}
			if( cnt === 111 ) {
				obj.flg[ yakuIndx["一気通貫"]] = true;
			}
		}
		if( Math.floor( obj.shuntsu[0] / 9 ) ===  Math.floor( obj.shuntsu[1] / 9 ) && Math.floor( obj.shuntsu[0] / 9 ) === Math.floor( obj.shuntsu[3] / 9 ) ) {
			var cnt = 0;
			var cntArray = new Array(0,1,3);
			for( var i = 0; i < cntArray.length; i++ ) {
				if( obj.shuntsu[cntArray[i]] % 9 === 0 ) cnt += 1;
				if( obj.shuntsu[cntArray[i]] % 9 === 3 ) cnt += 10;
				if( obj.shuntsu[cntArray[i]] % 9 === 6 ) cnt += 100;
			}
			if( cnt === 111 ) {
				obj.flg[ yakuIndx["一気通貫"]] = true;
			}
		}
		if( Math.floor( obj.shuntsu[0] / 9 ) ===  Math.floor( obj.shuntsu[2] / 9 ) && Math.floor( obj.shuntsu[0] / 9 ) === Math.floor( obj.shuntsu[3] / 9 ) ) {
			var cnt = 0;
			var cntArray = new Array(0,2,3);
			for( var i = 0; i < cntArray.length; i++ ) {
				if( obj.shuntsu[cntArray[i]] % 9 === 0 ) cnt += 1;
				if( obj.shuntsu[cntArray[i]] % 9 === 3 ) cnt += 10;
				if( obj.shuntsu[cntArray[i]] % 9 === 6 ) cnt += 100;
			}
			if( cnt === 111 ) {
				obj.flg[ yakuIndx["一気通貫"]] = true;
			}
		}
		if( Math.floor( obj.shuntsu[1] / 9 ) ===  Math.floor( obj.shuntsu[2] / 9 ) && Math.floor( obj.shuntsu[1] / 9 ) === Math.floor( obj.shuntsu[3] / 9 ) ) {
			var cnt = 0;
			var cntArray = new Array(1,2,3);
			for( var i = 0; i < cntArray.length; i++ ) {
				if( obj.shuntsu[cntArray[i]] % 9 === 0 ) cnt += 1;
				if( obj.shuntsu[cntArray[i]] % 9 === 3 ) cnt += 10;
				if( obj.shuntsu[cntArray[i]] % 9 === 6 ) cnt += 100;
			}
			if( cnt === 111 ) {
				obj.flg[ yakuIndx["一気通貫"]] = true;
			}
		}
	}
	//混全帯么九
	obj.flg[yakuIndx["混全帯么九"]] = true;
	if( !( obj.janto % 9 === 0 || obj.janto % 9 == 8 || Math.floor( obj.janto / 9 ) >= 3 ) ){
		obj.flg[yakuIndx["混全帯么九"]] = false;
	}
	for( var i = 0; i < obj.kotsu.length; i++ ) {
		if( !( ( obj.kotsu[i] % 9 === 0 || obj.kotsu[i] % 9 == 8 || Math.floor( obj.kotsu[i] / 9 )  >= 3 ) ) ) {
			obj.flg[yakuIndx["混全帯么九"]] = false;
		}
	}
	for( var i = 0; i < obj.shuntsu.length; i++) {
		if(  !( obj.shuntsu[i] % 9 === 0 || obj.shuntsu[i] % 9 === 6 )  ) {
			obj.flg[yakuIndx["混全帯么九"]] = false;
		}
	}

	//対々和
	if( obj.kotsu.length === 4 ) {
		obj.flg[yakuIndx["対々和"]] = true;
 	}
	//三暗刻
	if( obj.anko.length + obj.ankan.length === 3 ) {
		obj.flg[yakuIndx["三暗刻"]]  = true;
	}
	//混老頭
	if( obj.flg[yakuIndx["七対子"]] === true || obj.flg[yakuIndx["対々和"]] === true ) {
		for( var i = 0; i < obj.hand.length; i++ ) {
			if( !( obj.hand[i] % 9 === 0 || obj.hand[i] % 9 === 8 || Math.floor( obj.hand[i] / 9 ) >= 3 ) ) {
				break;
			}
			if( i === obj.hand.length - 1 ) {
				obj.flg[yakuIndx["混老頭"]] = true;
			}
		}
	}
	//三色同刻
	if( obj.kotsu.length === 3 ) {
		if( obj.kotsu[0] % 9 === obj.kotsu[1] % 9 && obj.kotsu[0] % 9 === obj.kotsu[2] % 9 && Math.floor( obj.kotsu[0] / 9 ) < 3 ) {
			obj.flg[yakuIndx["三色同刻"]] = true;
		}
	} else if( obj.kotsu.length === 4 ) {
		//012
		if( obj.kotsu[0] % 9 === obj.kotsu[1] % 9 && obj.kotsu[0] % 9 === obj.kotsu[2] % 9 && Math.floor( obj.kotsu[0] / 9 ) < 3 ) {
			obj.flg[yakuIndx["三色同刻"]] = true;
		}
		//013
		if( obj.kotsu[0] % 9 === obj.kotsu[1] % 9 && obj.kotsu[0] % 9 === obj.kotsu[3] % 9 && Math.floor( obj.kotsu[0] / 9 ) < 3 ) {
			obj.flg[yakuIndx["三色同刻"]] = true;
		}
		//023
		if( obj.kotsu[0] % 9 === obj.kotsu[2] % 9 && obj.kotsu[0] % 9 === obj.kotsu[3] % 9 && Math.floor( obj.kotsu[0] / 9 ) < 3 ) {
			obj.flg[yakuIndx["三色同刻"]] = true;
		}
		//123
		if( obj.kotsu[1] % 9 === obj.kotsu[2] % 9 && obj.kotsu[1] % 9 === obj.kotsu[3] % 9 && Math.floor( obj.kotsu[1] / 9 ) < 3 ) {
			obj.flg[yakuIndx["三色同刻"]] = true;
		}
	}
	//三槓子
	if( obj.ankan.length + obj.minkan.length === 3 ) {
		obj.flg[yakuIndx["三槓子"]] = true;
	}
	//小三元
	if( obj.freq[tileIndx["白"]] + obj.freq[tileIndx["發"]] + obj.freq[tileIndx["中"]] == 8 ) {
		obj.flg[yakuIndx["小三元"]] = true;

	}
	//両立直
	if( chkBoxState[1] === '1' ) {
		obj.flg[yakuIndx["両立直"]] = true;
	}

	//********三翻役********//
	//混一色
	var color = Math.floor( obj.hand[0] / 9 );
	for( var i = 0; i < obj.hand.length; i++ ) {
		if( !( ( Math.floor( obj.hand[i] / 9 ) === color ) || ( Math.floor( obj.hand[i] / 9 ) === 3 ) ) ) {
			break;
		}
		if( i === obj.hand.length - 1 ) {
			obj.flg[yakuIndx["混一色"]] = true;
		}
	}


	//純全帯么九
	obj.flg[yakuIndx["純全帯么九"]] = true;
	if( (!( obj.janto % 9 === 0 || obj.janto % 9 == 8 ) ) || Math.floor(obj.janto / 9) === 3 ){
		obj.flg[yakuIndx["純全帯么九"]] = false;
	}
	if( obj.flg["混老頭"] === true ) { obj.flg[yakuIndx["純全帯么九"]] = false };
	for( var i = 0; i < obj.kotsu.length; i++ ) {
		if( (!( ( obj.kotsu[i] % 9 === 0 || obj.kotsu[i] % 9 == 8 ) ) ) || Math.floor( obj.kotsu[i] / 9 ) === 3 ) {
			obj.flg[yakuIndx["純全帯么九"]] = false;
		}
	}
	for( var i = 0; i < obj.shuntsu.length; i++) {
		if(  !( obj.shuntsu[i] % 9 === 0 || obj.shuntsu[i] % 9 === 6 )  ) {
			obj.flg[yakuIndx["純全帯么九"]] = false;
		}
	}

	//二盃口
	if( obj.shuntsu.length === 4 ) {
		//0123
		if( obj.shuntsu[0] === obj.shuntsu[1] && obj.shuntsu[2] === obj.shuntsu[3] && obj.shuntsu[0] !== obj.shuntsu[2] ) {
			obj.flg[yakuIndx["二盃口"]] = true;
		}
		//0213
		if( obj.shuntsu[0] === obj.shuntsu[2] && obj.shuntsu[1] === obj.shuntsu[3] && obj.shuntsu[0] !== obj.shuntsu[1] ) {
			obj.flg[yakuIndx["二盃口"]] = true;
		}
		//0312
		if( obj.shuntsu[0] === obj.shuntsu[3] && obj.shuntsu[2] === obj.shuntsu[1] && obj.shuntsu[0] !== obj.shuntsu[2] ) {
			obj.flg[yakuIndx["二盃口"]] = true;
		}
	}

	//******満貫役******//
	if( chkBoxState[10] === '1' ) {
		obj.flg[yakuIndx["流し満貫"]] = true;
	}

	//******六翻役*****//

	//清一色
	var color = Math.floor( obj.hand[0] / 9 );
	for( var i = 0; i < obj.hand.length; i++ ) {
		if( color === 3 ) { break; }
		if( Math.floor( obj.hand[i] / 9 ) !== color ) { break; }
		if( i === obj.hand.length - 1 ) {
			obj.flg[yakuIndx["清一色"]] = true;
		}
	}

	//******役満******//

	//四暗刻
	if( ( obj.anko.length + obj.ankan.length ) === 4 ) {
		if( obj.agari !== obj.janto ) {
			obj.flg[ yakuIndx["四暗刻"] ] = true;
		} else {
			obj.flg[ yakuIndx["四暗刻単騎待ち"] ] = true;
		}
		obj.isYakuman = true;
	}

	//大三元
	if( obj.freq[tileIndx["白"]] + obj.freq[tileIndx["發"]] + obj.freq[tileIndx["中"]] === 9 ) {
		obj.flg[ yakuIndx["大三元"]] = true;
		obj.isYakuman = true;
	}

	//字一色
	obj.flg[ yakuIndx[ "字一色" ] ] = true;
	for( var i = 0; i < obj.hand.length; i++ ) {
		if( Math.floor( obj.hand[i] / 9 ) !== 3 ) {
			obj.flg[ yakuIndx[ "字一色" ] ] = false;
		}
	}
	if( obj.flg[ yakuIndx[ "字一色" ] ] === true ) {
		if( obj.flg[ yakuIndx[ "七対子"]] === true ) {
			obj.flg[ yakuIndx[ "字一色" ] ] = false;
			obj.flg[ yakuIndx[ "字一色(大七星)" ] ] = true;
		}
		obj.isYakuman = true;
	}

	//小四喜
	if( obj.freq[tileIndx["東"]] + obj.freq[tileIndx["南"]] + obj.freq[tileIndx["西"]] + obj.freq[tileIndx["北"]] === 11 ) {
		obj.flg[ yakuIndx["小四喜"] ] = true;
		obj.isYakuman = true;
	}
	//大四喜
	if( obj.freq[tileIndx["東"]] + obj.freq[tileIndx["南"]] + obj.freq[tileIndx["西"]] + obj.freq[tileIndx["北"]] === 12 ) {
		obj.flg[ yakuIndx["大四喜"] ] = true;
		obj.isYakuman = true;
	}
	//緑一色
	if( obj.kotsu.length > 0 ) {
		var greens = new Array(tileIndx["二索"], tileIndx["三索"], tileIndx["四索"],
				tileIndx["六索"], tileIndx["八索"], tileIndx["發"]);
		var cnt = 0;
		for( var i = 0; i < greens.length; i++ ) {
			cnt += obj.freq[greens[i]];
		}
		if( cnt === handmax ) {
			obj.flg[ yakuIndx["緑一色"]] = true;
			obj.isYakuman = true;
		}
	}
	//清老頭
	if( obj.kotsu.length === 4 ) {
		for( var i = 0; i < obj.kotsu.length; i++ ) {
			if( !( ( obj.kotsu[i] % 9 === 0 || obj.kotsu[i] % 9 === 8 ) &&  Math.floor( obj.kotsu[i] / 9 ) < 3 ) )  {
				break;
			}
			if( i === obj.kotsu.length - 1 ) {
				obj.flg[ yakuIndx["清老頭"]] = true;
				obj.isYakuman = true;
			}
		}
	}
	//四槓子/
	if( obj.ankan.length + obj.minkan.length === 4 ) {
		obj.flg[ yakuIndx["四槓子"]] = true;
		obj.isYakuman = true;
	}
	for( var i = 0; i < obj.freq.length; i++){
		obj.freq[obj.hand[i]] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++){
		obj.freq[obj.hand[i]] ++;
	}
	//九蓮宝燈
	color = Math.floor(obj.hand[0]/9);
	var nineGates = new Array(3,1,1,1,1,1,1,1,3);
	if( obj.flg[yakuIndx["清一色"]] === true ) {
		obj.flg[yakuIndx["九蓮宝燈"]] = true;
		if( obj.furoCnt > 0 ) { obj.flg[yakuIndx["九蓮宝燈"]] = false; }
		if( obj.ankan.length > 0 ) {
			obj.flg[yakuIndx["九蓮宝燈"]] = false;
		}
		for( var i = 0; i < nineGates.length; i++ ) {
			obj.freq[ color * 9 + i ] -= nineGates[i];
		}
		for( var i = 0; i < obj.freq.length; i++ ) {
			if( obj.freq[i] < 0 || obj.freq[i] > 1 ) {
				obj.flg[yakuIndx["九蓮宝燈"]] = false;
			}
		}
		if( obj.flg[yakuIndx["九蓮宝燈"]] === true ) {
			if( obj.freq[obj.agari] === 1 ) {
				obj.flg[yakuIndx["純正九蓮宝燈"]] = true;
				obj.flg[yakuIndx["九蓮宝燈"]] = false;
			}
			obj.isYakuman = true;
		}
	}
	//天和
	if( chkBoxState[8] === '1' && winds[1] === 0 ) {
		obj.flg[yakuIndx["十三不塔"]] = false;
		obj.flg[yakuIndx["十三無靠"]] = false;
		obj.flg[yakuIndx["天和"]] = true;
		obj.isYakuman = true;
	}
	//地和
	if( chkBoxState[9] === '1' && winds[1] !== 0  && obj.flg[yakuIndx["十三不塔"]] === false && obj.flg[yakuIndx["十三無靠"]] === false ) {
		obj.flg[yakuIndx["十三不塔"]] = false;
		obj.flg[yakuIndx["十三無靠"]] = false;
		obj.flg[yakuIndx["地和"]] = true;
		obj.isYakuman = true;
	}
}

/*	***********************************************
 *	******和了形からローカル役を読み取る関数*******
 *	***********************************************
 */
function checkLocalHands( obj ) {

	//脊髄
	if( chkBoxState_locals[1] === true ) {
		if( obj.furoCnt === 4 ) {
			obj.flg[yakuIndx["脊髄"]] = true;
			obj.isYakuman = true;
		}
	}

	//開立直
	if( chkBoxState_locals[2] === true ) {
		obj.flg[yakuIndx["開立直"]] = true;
		obj.flg[yakuIndx["立直"]] = false;
		yakulist[yakuIndx["開立直"]].points = 2;
		yakulist[yakuIndx["開立直"]].downpts = 2;
		if( obj.flg[yakuIndx["両立直"]] === true ) {
			yakulist[yakuIndx["開立直"]].points = 1;
			yakulist[yakuIndx["開立直"]].downpts = 1;
		}
	}

	//人和
	if( chkBoxState_locals[3] === true ) {
		obj.flg[yakuIndx["人和"]] = true;
	}

	//三連刻
	if( chkBoxState_locals[4] === true ) {
		var sanren = new Array();
		for( var i = 0; i < obj.kotsu.length; i++ ) {
			sanren.push( obj.kotsu[i] );
		}
		sanren.sort(
			function( a, b ) {
				if( a < b ) return -1;
				if( a > b ) return 1;
				return 0;
			}
		);

		for( var i = 0; i < sanren.length; i++ ) {
			if( sanren[i] % 9 >= 7 ) { continue; }
			if( i >= sanren.length - 1 - 1 ) { continue; }
			if( Math.floor( sanren[i] / 9 ) >= 3 ) { continue; }
			if( sanren[i] === sanren[i+1] - 1 &&
					sanren[i] === sanren[i+2] - 2 ) {
				obj.flg[yakuIndx["三連刻"]] = true;
			}
		}
	}

	//四連刻
	if( chkBoxState_locals[5] === true ) {
		var suren = new Array();
		for( var i = 0; i < obj.kotsu.length; i++ ) {
			suren.push( obj.kotsu[i] );
		}
		suren.sort(
			function( a, b ) {
				if( a < b ) return -1;
				if( a > b ) return 1;
				return 0;
			}
		);
		for( var i = 0; i < suren.length; i++ ) {
			if( suren[i] % 9 >= 6 ) { continue; }
			if( i >= suren.length - 1 - 2 ) { continue; }
			if( Math.floor( suren[i] / 9 ) >= 3 ) { continue; }
			if( suren[i] === sanren[i+1] - 1 &&
					suren[i] === suren[i+2] - 2 &&
					suren[i] === suren[i+3] - 3) {
				obj.flg[yakuIndx["四連刻"]] = true;
			}
		}
	}

	//大車輪
	if( chkBoxState_locals[6] === true ) {
		if( obj.flg[yakuIndx["断么九"]] === true &&
			obj.flg[yakuIndx["清一色"]] === true &&
			obj.flg[yakuIndx["七対子"]] === true ) {
			var color = Math.floor( obj.hand[0] / 9 );
			if( color === 1 ) {
				obj.flg[yakuIndx["大車輪"]] = true;
				obj.isYakuman = true;
			}
		}
	}

	//八連荘
	if( chkBoxState_locals[7] === true ) {
		obj.flg[yakuIndx["八連荘"]] = true;
		obj.isYakuman = true;
	}

	//一色三順
	if( chkBoxState_locals[8] === true ) {
		if( obj.shuntsu.length == 3 ) {
			if( obj.shuntsu[0] === obj.shuntsu[1] && obj.shuntsu[0] === obj.shuntsu[2] ) {
				obj.flg[yakuIndx["一色三順"]] = true;
			}
		} else if( obj.shuntsu.length === 4 ) {
			//012
			if( obj.shuntsu[0] === obj.shuntsu[1] && obj.shuntsu[0] === obj.shuntsu[2] ) {
				obj.flg[yakuIndx["一色三順"]] = true;
			}
			//023
			if( obj.shuntsu[0] === obj.shuntsu[2] && obj.shuntsu[0] === obj.shuntsu[3] ) {
				obj.flg[yakuIndx["一色三順"]] = true;
			}
			//013
			if( obj.shuntsu[0] === obj.shuntsu[1] && obj.shuntsu[0] === obj.shuntsu[3] ) {
				obj.flg[yakuIndx["一色三順"]] = true;
			}
			//123
			if( obj.shuntsu[2] === obj.shuntsu[1] && obj.shuntsu[3] === obj.shuntsu[2] ) {
				obj.flg[yakuIndx["一色三順"]] = true;
			}
		}
	}

	//一色四順
	if( chkBoxState_locals[9] === true ) {
		if( obj.shuntsu.length == 4 ) {
			if( obj.shuntsu[0] === obj.shuntsu[1] && obj.shuntsu[1] === obj.shuntsu[2] && obj.shuntsu[2] === obj.shuntsu[3] ) {
				obj.flg[yakuIndx["一色四順"]] = true;
				obj.isYakuman = true;
			}
		}
	}

	//ベンツ
	if( chkBoxState_locals[10] === true ) {
		for( var i = 0; i < obj.ankan.length; i++ ) {
			if( obj.ankan[i] === tileIndx["八索"] ) {
				obj.flg[yakuIndx["ベンツ"]] = true;
			}
		}
		for( var i = 0; i < obj.minkan.length; i++ ) {
			if( obj.minkan[i] === tileIndx["八索"] ) {
				obj.flg[yakuIndx["ベンツ"]] = true;
			}
		}
	}

	//新幹線
	if( chkBoxState_locals[11] === true ) {
		for( var i = 0; i < obj.ankan.length; i++ ) {
			if( obj.ankan[i] === tileIndx["八筒"] ) {
				obj.flg[yakuIndx["新幹線"]] = true;
			}
		}
		for( var i = 0; i < obj.minkan.length; i++ ) {
			if( obj.minkan[i] === tileIndx["八筒"] ) {
				obj.flg[yakuIndx["新幹線"]] = true;
			}
		}
	}

}

/*	***********************************************
 *	*****上位役と下位役の重複を解消する************
 *	***********************************************
 */
function checkYakuCompounded( obj ) {

	if( chkBoxState_options[0] === true ) { return; }

	if( obj.flg[ yakuIndx["嶺上開花"]] === true ) {
		obj.flg[ yakuIndx["一発"]] = false;
	}

	if( obj.flg[yakuIndx["両立直"]] === true ) {
		obj.flg[yakuIndx["立直"]] = false;
	}

	if( obj.flg[yakuIndx["二盃口"]] === true ) {
		obj.flg[yakuIndx["一盃口"]] = false;
	}

	if( obj.flg[yakuIndx["清一色"]] === true ) {
		obj.flg[yakuIndx["混一色"]] = false;
	}

	if( obj.flg[yakuIndx["純全帯么九"]] === true ) {
		obj.flg[yakuIndx["混全帯么九"]] = false;
	}

	if( obj.flg[yakuIndx["混老頭"]] === true ) {
		obj.flg[yakuIndx["混全帯么九"]] = false;
		obj.flg[yakuIndx["純全帯么九"]] = false;
	}

	if( obj.flg[yakuIndx["四連刻"]] === true ) {
		obj.flg[yakuIndx["三連刻"]] = false;
	}

	if( obj.flg[yakuIndx["一色三順"]] === true ) {
		obj.flg[yakuIndx["一盃口"]] = false;
	}

	if( obj.flg[yakuIndx["一色四順"]] === true ) {
		 obj.flg[yakuIndx["一色三順"]] = false;
		obj.flg[yakuIndx["一盃口"]] = false;
	}

	if( obj.flg[yakuIndx["国士無双十三面待ち"]] === true ) {
		obj.flg[yakuIndx["国士無双"]] = false;
	}

	if( obj.flg[yakuIndx["国士無双"]] === true || obj.flg[yakuIndx["国士無双十三面待ち"]] === true ||
			obj.flg[yakuIndx["天和"]] === true || obj.flg[yakuIndx["地和"]] === true ) {
		obj.flg[yakuIndx["十三不塔"]] = false;
		obj.flg[yakuIndx["十三無靠"]] = false;
	}

}

/*	***********************************************
 *	******役満に関する処理*************************
 *	***********************************************
 */


function checkYakuman( obj ) {
	if( chkBoxState_options[1] === true ) { return; }
	if( obj.isYakuman == true ) {
		for( var i = 0; i < obj.flg.length; i++ ) {
			if( yakulist[i].points < 13 ) {
				obj.flg[i] = false;
			}
		}
	}
}
function checkNagashi( obj ) {
	if( obj.flg[yakuIndx["流し満貫"]] === true ) {
		for( var i = 0; i < obj.flg.length; i++ ) {
			if( i === yakuIndx["流し満貫"] ) { continue; }
			obj.flg[i] = false;
		}
	}
}

/*	***********************************************
 *	******翻数を計算する関数***********************
 *	***********************************************
 */

function getHan( obj ) {
	var tmp;
	if( obj.isFuro === true ) {
		tmp = 1;
	} else {
		tmp = 0;
	}
	obj.han = 0;	//食い下がりを考慮する
	obj.hanstr = "";
	for( var i = 0; i < obj.flg.length; i++ ) {
		if( obj.flg[i] === true && yakulist[i].points - tmp * yakulist[i].downpts > 0 ) {
			obj.han += yakulist[i].points - tmp * yakulist[i].downpts;
			obj.hanstr += yakulist[i].name + " " + ( yakulist[i].points - tmp * yakulist[i].downpts ) + "翻\n";
		} else if( yakulist[i].points - tmp * yakulist[i].downpts <= 0 ) {
			obj.flg[i] = false;
		}
	}

	//新幹線が成立する場合
	if( obj.flg[yakuIndx["新幹線"]] === true ) {
		obj.han *= 3;
	}

	if( obj.han === 0 ) {
		obj.hanstr = "役なし(形式聴牌)\n";
		return;
	}

	for( var i = 0; i < obj.freq.length; i++) {
		obj.freq[i] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++ ) {
		obj.freq[obj.hand[i]] ++;
	}
	for( var i = 0; i < obj.ankan.length; i++ ) {
		obj.freq[obj.ankan[i]] = 4;
	}
	for( var i = 0; i < obj.minkan.length; i++ ) {
		obj.freq[obj.minkan[i]] = 4;
	}

	if( obj.isYakuman === true && chkBoxState_options[1] !== true ) { return; }
	if( obj.flg[yakuIndx["流し満貫"]] === true ) { return; }

	//ドラ計算
	//ふつうのドラ
	var dracnt = 0;
	for( var i = 0; i < dragons.length; i++ ) {
		//ドラ表示牌からドラを調べる
		var dra;
		if( dragons[i] === tilemax ) { continue; }
		if( Math.floor( dragons[i] / 9 ) < 3 ) {
			dra = Math.floor( dragons[i] / 9 ) * 9 + ( dragons[i] + 1 ) % 9;
		} else {
			if( dragons[i] <= tileIndx["北"] && dragons[i] >= tileIndx["東"] ) {
				dra = tileIndx["東"] + ( dragons[i] - tileIndx["東"] + 1 ) % 4;
			} else if( dragons[i] >= tileIndx["白"] && dragons[i] <= tileIndx["中"] ) {
				dra = tileIndx["白"] + ( dragons[i] - tileIndx["白"] + 1 ) % 3;
			}
		}
		dracnt += obj.freq[dra];
	}

	//ベンツの場合の処理
	if( obj.flg[yakuIndx["ベンツ"]] === true ) {
		dracnt *= 3;
	}

	obj.han += dracnt;
	obj.hanstr += "ドラ " + dracnt + "翻\n";

	//赤ドラ
	var redDraCnt = 0;
	redDraCnt = new Number( document.etcpts.redDragons.value );
	if( obj.flg[yakuIndx["ベンツ"]] === true ) {
		redDraCnt *= 3;
	}
	if( redDraCnt > 0 ) {
		obj.hanstr += "赤ドラ " + redDraCnt + "翻\n";
		obj.han += redDraCnt;
	}

	//北ドラ
	var northDraCnt = 0;
	northDraCnt = new Number( document.etcpts.northDragons.value );
	if( obj.flg[yakuIndx["ベンツ"]] === true ) {
		northDraCnt *= 3;
	}
	if( northDraCnt > 0 && chkBoxState[12] === '1' ) {
		obj.hanstr += "北ドラ " + northDraCnt + "翻\n";
		obj.han += northDraCnt;
	}

	//花ドラ
	var flowerDraCnt = 0;
	flowerDraCnt = new Number( document.etcpts.flowerDragons.value );
	if( obj.flg[yakuIndx["ベンツ"]] === true ) {
		flowerDraCnt *= 3;
	}
	if( flowerDraCnt > 0 ) {
		obj.hanstr += "花ドラ " + flowerDraCnt + "翻\n";
		obj.han += flowerDraCnt;
	}


}

/*	***********************************************
 *	********符を計算する関数***********************
 *	***********************************************
 */
function getFu( obj ) {
	obj.fu = 0;		//符の初期化
	obj.fustr = "";

	//雀頭を出力する
	if( obj.flg[yakuIndx["国士無双"]] !== true && obj.flg[yakuIndx["国士無双十三面待ち"]] !== true
			&& obj.flg[yakuIndx["七対子"]] !== true && obj.flg[yakuIndx["十三不塔"]] !== true && obj.flg[yakuIndx["十三無靠"]] !== true ) {
		obj.fustr += "雀頭 " + tilename[obj.janto] + "\n";
	}
	//待ちの名称を出力する
	if( obj.machi >= 0 ) { obj.fustr += machiName[obj.machi] + "\n\n"; }



	//七対子の場合
	if( obj.flg[yakuIndx["七対子"]] === true ) {
		obj.fu = 25;
		obj.fustr += "七対子 25符\n"
		return;
	}
	//平和
	if( obj.flg[yakuIndx["平和"]] === true ) {
		if( obj.isFuro === false ) {
			obj.fu = 20;
			obj.fustr += "副底(平和) 20符\n"
			//門前加符
			if( obj.isFuro === false && chkBoxState[3] !== '1' ) {
				obj.fu += 10;
				obj.fustr += "門前加符 10符\n"
			}
		} else {
			obj.fu = 30;
			obj.fustr += "喰い平和 30符\n"
		}
		return;
	}

	obj.fu = 20;	//副底
	obj.fustr += "副底 20符\n"

	//明刻
	for( var i = 0; i < obj.minko.length; i++ ) {
		if( obj.minko[i] % 9 === 0 || obj.minko[i] % 9 === 8 || Math.floor(obj.minko[i] / 9) === 3 ) {
			obj.fu += 4;
			obj.fustr += "明刻 " + tilename[obj.minko[i]] + " 4符\n";
		} else {
			obj.fu += 2;
			obj.fustr += "明刻 " + tilename[obj.minko[i]] + " 2符\n";
		}
	}
	//暗刻
	for( var i = 0; i < obj.anko.length; i++ ) {
		if( obj.anko[i] % 9 === 0 || obj.anko[i] % 9 === 8 || Math.floor(obj.anko[i] / 9) === 3 ) {
			obj.fu += 8;
			obj.fustr += "暗刻 " + tilename[obj.anko[i]] + " 8符\n";
		} else {
			obj.fu += 4;
			obj.fustr += "暗刻 " + tilename[obj.anko[i]] + " 4符\n";
		}
	}
	//明槓
	for( var i = 0; i < obj.minkan.length; i++ ) {
		if( obj.minkan[i] % 9 === 0 || obj.minkan[i] % 9 === 8 || Math.floor(obj.minkan[i] / 9) === 3 ) {
			obj.fu += 16;
			obj.fustr += "明槓 " + tilename[obj.minkan[i]] + " 16符\n";
		} else {
			obj.fu += 8;
			obj.fustr += "明槓 " + tilename[obj.minkan[i]] + " 8符\n";
		}
	}
	//暗槓
	for( var i = 0; i < obj.ankan.length; i++ ) {
		if( obj.ankan[i] % 9 === 0 || obj.ankan[i] % 9 === 8 || Math.floor(obj.ankan[i] / 9) === 3 ) {
			obj.fu += 32;
			obj.fustr += "暗槓 " + tilename[obj.ankan[i]] + " 32符\n";
		} else {
			obj.fu += 16;
			obj.fustr += "暗槓 " + tilename[obj.ankan[i]] + " 16符\n";
		}
	}

	//雀頭に対する符
	if( obj.janto === winds[0] + tileIndx["東"] && obj.janto === winds[1] + tileIndx["東"] ) {
		obj.fu += 4;
		obj.fustr += "雀頭(連風牌) " + "4符\n";
	} else if( obj.janto === winds[0] + tileIndx["東"] ) {
		obj.fu += 2;
		obj.fustr += "雀頭(場風) " + "2符\n";
	} else if( obj.janto === winds[1] + tileIndx["東"] ) {
		obj.fu += 2;
		obj.fustr += "雀頭(自風) " + "2符\n";
	} else if( obj.janto >= tileIndx["白"] ) {
		obj.fu += 2;
		obj.fustr += "雀頭(三元牌) " + "2符\n";
	}


	//待ちに対する符
	if( obj.machi == machiIndx["嵌張待ち"] ) {
		obj.fu += 2;
		obj.fustr += "嵌張待ち 2符\n";
	} else if( obj.machi == machiIndx["辺張待ち"] ) {
		obj.fu += 2;
		obj.fustr += "辺張待ち 2符\n";
	} else if( obj.machi == machiIndx["単騎待ち"] ) {
		obj.fu += 2;
		obj.fustr += "単騎待ち 2符\n";
	}

	//門前加符
	if( obj.isFuro === false && chkBoxState[3] !== '1' ) {
		obj.fu += 10;
		obj.fustr += "門前加符 10符\n"
	}
	//ツモ符
	if( chkBoxState[3] === '1' ) {
		obj.fu += 2;
		obj.fustr += "ツモ符 2符\n";
	}

	//切り上げ
	if( obj.fu % 10 > 0 ) {
		obj.fu = Math.floor( obj.fu / 10 ) * 10 + 10;
	}

}

function resetObjFreq( obj ) {
	for( var i = 0; i < obj.freq.length; i++ ) {
		obj.freq[i] = 0;
	}
	for( var i = 0; i < obj.hand.length; i++ ) {
		obj.freq[obj.hand[i]] ++;
	}
}

/*	***********************************************
 *	********点数を計算する関数*********************
 *	***********************************************
 */
var pts_limits = new Array( 2000, 3000, 4000, 6000, 8000 );
var pts_limits_Indx = new Array();
pts_limits_Indx["満貫"] = 0;
pts_limits_Indx["跳満"] = 1;
pts_limits_Indx["倍満"] = 2;
pts_limits_Indx["三倍満"] = 3;
pts_limits_Indx["役満"] = 4;

function getPts( obj ) {
	var honba = new Number( document.etcpts.honba.value );
	obj.pts = 0;
	obj.ptsstr = obj.fu + "符 " + obj.han + "翻 " + honba + "本場\n";

	//基本点の算出
	obj.pts = obj.fu * Math.pow(2, ( obj.han + 2 ) );

	//上限をかける
	if( obj.han <= 5 && obj.pts > pts_limits[ pts_limits_Indx["満貫"]] ) {
		obj.pts = pts_limits[ pts_limits_Indx["満貫"]];
		obj.ptsstr += "満貫\n";
	} else if( obj.han >= 6 && obj.han <= 7 ){
		obj.pts = pts_limits[ pts_limits_Indx["跳満"]];
		obj.ptsstr += "跳満\n";
	} else if( obj.han >= 8 && obj.han <= 10 ) {
		obj.pts = pts_limits[ pts_limits_Indx["倍満"]];
		obj.ptsstr += "倍満\n";
	} else if( obj.han >= 11 && obj.han <= 12 ) {
		obj.pts = pts_limits[ pts_limits_Indx["三倍満"]];
		obj.ptsstr += "三倍満\n";
	} else if( obj.han >= 13 ) {
		if( Math.floor(obj.han/13) === 1 ) {
			obj.ptsstr += "役満\n";
		} else {
			if( chkBoxState_options[3] !== true ) {
				obj.ptsstr += Math.floor(obj.han/13) + "倍役満\n";
			} else {
				obj.ptsstr += Math.floor(obj.han/10) + "倍役満\n";
			}
		}

		obj.pts = pts_limits[ pts_limits_Indx["役満"]];
		if( chkBoxState_options[2] === true ) {
			if( chkBoxState_options[3] !== true ) {
				obj.pts = pts_limits[ pts_limits_Indx["役満"]] * Math.floor( obj.han / 13 );
			} else {
				obj.pts = pts_limits[ pts_limits_Indx["役満"]] * Math.floor( obj.han / 10 );
			}
		}
	}


	var childpts = 0;
	var parentpts = 0;

	if( winds[1] === 0 ) {

		if( chkBoxState[3] === '1' ) {
			//親のツモ和了
			childpts = obj.pts * 2;
			if( childpts % 100 > 0 ) {
				childpts = Math.floor( childpts / 100 ) * 100 + 100;
			}
			childpts += 100 * honba;
			if( chkBoxState[12] === '1' ) {
				obj.ptsstr += childpts * 2 + "点\n";
			} else {
				obj.ptsstr += childpts * 3 + "点\n";
			}
			obj.ptsstr += childpts + "点オール\n";
		} else {
			//親のロン和了
			childpts = obj.pts * 6;
			if( childpts % 100 > 0 ) {
				childpts = Math.floor( childpts / 100 ) * 100 + 100;
			}
			childpts += 300 * honba;
			obj.ptsstr += childpts + "点\n";
		}

	} else {

		if( chkBoxState[3] === '1' ) {
			//子のツモ和了
			parentpts = obj.pts * 2;
			childpts = obj.pts;
			if( childpts % 100 > 0 ) {
				childpts = Math.floor( childpts / 100 ) * 100 + 100;
			}
			if( parentpts % 100 > 0 ) {
				parentpts = Math.floor( parentpts / 100 ) * 100 + 100;
			}
			parentpts += 100 * honba;
			childpts += 100 * honba;
			if( chkBoxState[12] === '1' ) {
				obj.ptsstr += childpts + parentpts + "点\n";
			} else {
				obj.ptsstr += childpts * 2 + parentpts + "点\n";
			}
			obj.ptsstr += childpts + "-" + parentpts + "\n";
		} else {
			//子のロン和了
			childpts = obj.pts * 4;
			if( childpts % 100 > 0 ) {
				childpts = Math.floor( childpts / 100 ) * 100 + 100;
			}
			childpts += 300 * honba;
			obj.ptsstr += childpts + "点\n";
		}

	}


}

/*	***********************************************
 *	********待ち牌を検出する関数*******************
 *	***********************************************
 */
var possibleList = new Array();
function checkMachiTile(){
	possibleList = new Array();
	var showstr = "待ち牌は\n";
	hand[hand.length - 1] = tilemax;
	for( var i = 0; i < tilemax; i++ ) {
		hand[hand.length - 1] = i;
		if( searchHands( true ) === true ) {
			showstr += tilename[i] + "\n";
		}
	}
	hand[hand.length - 1] = tilemax;
	if( possibleList.length === 0 ) {

		showstr = "待ち牌なし\n";
	}
	for( var i = 0; i < possibleList.length ; i++ ) {
		showstr += "\n■■■■■" + tilename[possibleList[i].agari] + "の場合■■■■■\n\n"
		showstr += possibleList[i].ptsstr + "\n" + possibleList[i].hanstr + "\n" +  possibleList[i].fustr;
	}
	document.outputbox.outbox.value = showstr;
}


var yakucnt = 0;
var yakuIndx = new Array();
//******役を意味するオブジェクト********
function yaku( name, points, downpts ){
/*
 * @param
 * name:麻雀役の名称
 * points:翻数
 * downpts:食い下がり翻数
 */
	this.name = name;
	this.points = points;
	this.downpts = downpts;
	yakuIndx[name] = yakucnt;
	yakucnt ++;
}

var yakulist = new Array();
//********役を定義する関数*******
function setYaku() {

	//*********一翻役*********
	yakulist.push( new yaku("立直", 1, 1  ) );
	yakulist.push( new yaku("一発", 1, 1) );
	yakulist.push( new yaku("門前清自摸和", 1, 1) );
	yakulist.push( new yaku("断么九",1, 0) );
	yakulist.push( new yaku("平和", 1, 1) );
	yakulist.push( new yaku("一盃口", 1, 1) );
	yakulist.push( new yaku("三元牌　白", 1, 0 ) );
	yakulist.push( new yaku("三元牌　發", 1, 0) );
	yakulist.push( new yaku("三元牌　中", 1, 0) );
	yakulist.push( new yaku("場風　東", 1, 0) );
	yakulist.push( new yaku("場風　南", 1, 0) );
	yakulist.push( new yaku("場風　西", 1, 0) );
	yakulist.push( new yaku("場風　北", 1, 0) );
	yakulist.push( new yaku("自風　東", 1, 0) );
	yakulist.push( new yaku("自風　南", 1, 0) );
	yakulist.push( new yaku("自風　西", 1, 0) );
	yakulist.push( new yaku("自風　北", 1, 0) );
	yakulist.push( new yaku("嶺上開花", 1, 0) );
	yakulist.push( new yaku("槍槓", 1, 0) );
	yakulist.push( new yaku("海底摸月", 1, 0) );
	yakulist.push( new yaku("河底撈魚", 1, 0) );

	//********二翻役********
	yakulist.push( new yaku("三色同順", 2, 1) );
	yakulist.push( new yaku("一気通貫", 2, 1) );
	yakulist.push( new yaku("混全帯么九", 2, 1) );
	yakulist.push( new yaku("七対子", 2, 2 ) );
	yakulist.push( new yaku("対々和", 2, 0) );
	yakulist.push( new yaku("三暗刻", 2, 0) );
	yakulist.push( new yaku("混老頭", 2, 0) );
	yakulist.push( new yaku("三色同刻", 2, 0) );
	yakulist.push( new yaku("三槓子", 2, 0) );
	yakulist.push( new yaku("小三元", 2, 0) );
	yakulist.push( new yaku("両立直", 2, 2) );

	//*******三翻役*******//
	yakulist.push( new yaku("混一色", 3, 1) );
	yakulist.push( new yaku("純全帯么九", 3, 1) );
	yakulist.push( new yaku("二盃口", 3, 0) );

	//******満貫役******//
	yakulist.push( new yaku("流し満貫", 5, 0) );

	//******六翻役********//
	yakulist.push( new yaku("清一色", 6, 1) );

	//******役満********//
	yakulist.push( new yaku("国士無双", 13, 13) );
	yakulist.push( new yaku("国士無双十三面待ち", 13, 13) );
	yakulist.push( new yaku("四暗刻", 13, 13) );
	yakulist.push( new yaku("四暗刻単騎待ち", 13, 13) );
	yakulist.push( new yaku("大三元", 13, 0) );
	yakulist.push( new yaku("字一色", 13, 0) );
	yakulist.push( new yaku("字一色(大七星)", 13, 0) );
	yakulist.push( new yaku("小四喜", 13, 0) );
	yakulist.push( new yaku("大四喜", 13, 0) );
	yakulist.push( new yaku("緑一色", 13, 0) );
	yakulist.push( new yaku("清老頭", 13, 0) );
	yakulist.push( new yaku("四槓子", 13, 0) );
	yakulist.push( new yaku("九蓮宝燈", 13, 13) );
	yakulist.push( new yaku("純正九蓮宝燈", 13, 13) );
	yakulist.push( new yaku("天和", 13, 13) );
	yakulist.push( new yaku("地和", 13, 13) );

	//*******ローカル役*******//
	yakulist.push( new yaku("三連刻", 2, 0) );
	yakulist.push( new yaku("四連刻", 13, 0) );
	yakulist.push( new yaku("大車輪", 13, 13) );
	yakulist.push( new yaku("八連荘", 13, 0) );
	yakulist.push( new yaku("人和", 13, 13) );
	yakulist.push( new yaku("十三不塔", 13, 13) );
	yakulist.push( new yaku("十三無靠", 13, 13) );
	yakulist.push( new yaku("脊髄", 13, 0) );
	yakulist.push( new yaku("開立直", 13, 0) );
	yakulist.push( new yaku("一色三順", 3, 1) );
	yakulist.push( new yaku("一色四順", 13, 0) );
	yakulist.push( new yaku("ベンツ", 2, 0) );
	yakulist.push( new yaku("新幹線", 1, 0) );

}

//*********ループ処理*********
var timer;
var delay = 1000/60;
function loop(){
	redraw();
	getMouseState();
	select_hands();
	setFuro();
	draw();
    clearTimeout( timer );
    timer = setTimeout( loop, delay );
}

//********メイン関数******
function main() {
	load();
	loop();
}

main();